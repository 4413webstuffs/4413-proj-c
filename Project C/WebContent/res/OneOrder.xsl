<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:template match="/">
    <html>
    <head>
      <title>Purchase Summary</title>
    </head>
    <body>
	<xsl:apply-templates/>
    </body>
    </html>
    </xsl:template>

    <xsl:template match="order">
	<h1>Order Summary</h1>
	<p>
	Customer: <xsl:value-of select="customer/name" /><br/>
	ID Number: <xsl:value-of select="@id"/><br/>
	Date Submitted: <xsl:value-of select="@submitted"/><br/>
	</p>
	
	<table border="1">
	<tr>
		<td>PRODUCT NAME</td>
		<td>NUMBER</td>
		<td>PRICE</td>
		<td>QUANTITY</td>
		<td>TOTAL</td>
	</tr>
	<xsl:apply-templates/>
	</table>
    </xsl:template>
    
    <xsl:template match="customer">
    </xsl:template>
    
    <xsl:template match="items">
	<tr>
	 	<td><xsl:value-of select="./name"/></td>
		<td>#<xsl:value-of select="@number"/></td>
	  	<td>$<xsl:value-of select="./price"/></td>
	  	<td><xsl:value-of select="./quantity"/></td>
	  	<td>$<xsl:value-of select="./extended"/></td>
	</tr>
    </xsl:template>
    
    <xsl:template match="total">
    	<tr>
    		<td border="0" colspan="3"></td>
    		<td>Total:</td>
    		<td>$<xsl:value-of select="format-number(., '0.##')"/></td>
    	</tr>
    </xsl:template>
    <xsl:template match="shipping">
    	<tr>
    		<td border="0" colspan="3"></td>
    		<td>Shipping:</td>
    		<td>$<xsl:value-of select="format-number(., '0.##')"/></td>
    	</tr>
    </xsl:template>
    <xsl:template match="HST">
    	<tr>
    		<td border="0" colspan="3"></td>
    		<td>HST:</td>
    		<td>$<xsl:value-of select="format-number(., '0.##')"/></td>
    	</tr>
    </xsl:template>
     <xsl:template match="grandTotal">
    	<tr>
    		<td border-color="white" colspan="3"></td>
    		<td>Grand Total:</td>
    		<td>$<xsl:value-of select="format-number(., '0.##')"/></td>
    	</tr>
    </xsl:template>
    
    
</xsl:stylesheet>