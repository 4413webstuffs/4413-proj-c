package auth;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import com.sun.net.ssl.HttpsURLConnection;

import model.Customer;

public class HttpAuth
{
	public String login(String user, String password) throws Exception
	{
		String url = "https://www.eecs.yorku.ca/~cse13192/4413/auth.cgi";
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
 
		con.setRequestMethod("POST");
		con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
		
		String userPassword = user + ":" + password;
		String encoding = new sun.misc.BASE64Encoder().encode (userPassword.getBytes());
		con.setRequestProperty ("Authorization", "Basic " + encoding);
 
		String urlParameters = "user=" + user;
 
		// Send post request
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();
 
		int responseCode = con.getResponseCode();
		System.out.println("\nSending 'POST' request to URL : " + url);
		System.out.println("Post parameters : " + urlParameters);
		System.out.println("Response Code : " + responseCode);
 
		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();
 
		while ((inputLine = in.readLine()) != null)
		{
			response.append(inputLine);
		}
		in.close();
 
		if(responseCode == 200)
			//System.out.println("Name " + response.toString());
			return response.toString();
		else
			return null;
	}
}
