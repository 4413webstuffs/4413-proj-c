package b2b;

import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.LinkedList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.soap.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;

import model.*;

/**Unfinished. Could not be completed. Ideas on it in report**/

public class b2b
{
	private final static String YYZ = "http://red.cse.yorku.ca:4413/axis/YYZ.jws"; // Toronto
	private final static String YHX = "http://red.cse.yorku.ca:4413/axis/YHZ.jws"; // Halifax
	private final static String YVR = "http://red.cse.yorku.ca:4413/axis/YVR.jws"; // Vancouver
	
	private final static String url = "http://localhost:4413/Project_C/export/";
	
	private final String orderKey = "4413secret";
	
	private static String[] tnsArr = {YYZ, YHX, YVR};
	private static String[] tnsHuman = {"Toronto", "Halifax", "Vancouver"};
	
	public static void main(String []args)
	{
		int minIndex = getMinIndex();
		
		
	}
	
	public static int getMinIndex()
	{
		int min = 0;
		try
		{
			double quotes []= {0, 0, 0};
			for (ProcOrder pr: join(orders(files())))
			{
				for(int i = 0; i < 3; i++)
				{
					quotes[i] = getQuote(tnsArr[i], pr.number);
					if(i > 0 && quotes[min] > quotes[i])
						min = i;
				}				
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return min;
	}
	
	public static LinkedList<ProcOrder> join(LinkedList<List<CartItem>> original) throws Exception
	{
		LinkedList<ProcOrder> result = new LinkedList<ProcOrder>();
		for(List<CartItem> e : original)
		{
			for(CartItem c : e)
			{
				listAddition(result,
						new ProcOrder(c.getNumber(), c.getQty()));
			}
		}
		
		return result;
	}
	
	private static void listAddition(LinkedList<ProcOrder> list, ProcOrder procOrder) 
	{
		for (ProcOrder proc : list) 
		{
			if (proc.number.equals(procOrder.number)) 
			{
				proc.quantity = proc.quantity + procOrder.quantity;
			}
		}
		
	}
	
	private static LinkedList<String> files() throws Exception
	{
		LinkedList<String> result = new LinkedList<String>();
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		con.connect();
		BufferedReader inp = new BufferedReader(new InputStreamReader(
				con.getInputStream()));
		String line;
		for (; (line = inp.readLine()) != null;) {
			if (line.contains(".xml")) {
				String tmpFileName;
				tmpFileName = line.substring(line.indexOf("<tt>") + 4,
						line.indexOf("</tt>"));
				result.add(tmpFileName);
			}
		}
		return result;
	}
	
	private static LinkedList<List<CartItem>> orders(LinkedList<String> list) throws Exception
	{
		LinkedList<List<CartItem>> result = new LinkedList<List<CartItem>>();
		
		for (String token : list) 
		{
			URL furl = new URL(url + token);
			URLConnection con = furl.openConnection();
			con.connect();
			JAXBContext jc = JAXBContext.newInstance(OrderProducer.Order.class);

			Unmarshaller unmarshaller = jc.createUnmarshaller();
			OrderProducer.Order temp = (OrderProducer.Order) unmarshaller.unmarshal(con
					.getInputStream());
			result.add(temp.getTheItems());
		}
		return result;	
	}
	
	public static double getQuote(String tns, String number) throws Exception
	{

		double price = 0.0;

		SOAPMessage msg;
		try
		{
			msg = MessageFactory.newInstance().createMessage();
			MimeHeaders header = msg.getMimeHeaders();
			header.addHeader("SOAPAction", "");

			SOAPPart soap = msg.getSOAPPart();
			SOAPEnvelope envelope = soap.getEnvelope();
			SOAPBody body = envelope.getBody();

			body.addChildElement("quote").addChildElement("itemNumber")
					.addTextNode(number);

			SOAPConnection sc = SOAPConnectionFactory.newInstance()
					.createConnection();
			SOAPMessage resp = sc.call(msg, new URL(tns));
			sc.close();

			org.w3c.dom.Node node = resp.getSOAPPart().getEnvelope().getBody()
					.getElementsByTagName("quoteReturn").item(0);

			price = Double.parseDouble(node.getTextContent());

		} catch (Exception e)
		{

			e.printStackTrace();
			throw new Exception("Could not retrieve quote");
		}

		return price;
	}
	
	
	
	@XmlRootElement(name="orders")
	static class bigPO
	{
		@XmlElement
		List<ProcOrder> orderRequest;
		@XmlElement
		String tns;
		
		public bigPO()
		{
			//Do not use
		}
		public bigPO(List<ProcOrder> thing, String otherThing)
		{
			this.orderRequest = thing;
			this.tns = otherThing;
		}
	}
	
	static class ProcOrder
	{
		@XmlElement
		String number;
		@XmlElement
		int quantity;
		
		public ProcOrder()
		{
			//DO NOT USE!
		}
		
		public ProcOrder(String num, int qty)
		{
			this.number = num;
			this.quantity = qty;
		}
	}
}
