package control;

import java.io.IOException;
import java.util.LinkedList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.CartItem;

/**
 * Servlet implementation class Cart
 */
public class Cart extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Cart()
    {
        super();
        // TODO Auto-generated constructor stub
    }

    @Override
	public void init() throws ServletException
	{
		System.out.println("Cart-init");
		super.init();
	}
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		HttpSession sn = request.getSession();
		LinkedList<CartItem> cart;
		if(sn.getAttribute("cart") == null)
		{
			cart = new LinkedList<CartItem>();
			sn.setAttribute("cart", cart);
		}
		else
		{
			@SuppressWarnings("unchecked")
			LinkedList<CartItem> attribute = (LinkedList<CartItem>) sn.getAttribute("cart");
			cart = attribute;
		}
		request.setAttribute("cart", cart);
		
		String target = "/Front.jspx";
		request.setAttribute("target", "Cart.jspx");
		RequestDispatcher rd = request.getRequestDispatcher(target);
		rd.forward(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		this.doGet(request, response);
	}

}
