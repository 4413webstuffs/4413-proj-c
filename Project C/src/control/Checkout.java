package control;

import java.io.IOException;
import java.util.LinkedList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import auth.HttpAuth;
import model.CartItem;
import model.Customer;
import model.OrderProducer;

/**
 * Servlet implementation class Checkout
 */
@WebServlet("/Checkout")
public class Checkout extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Checkout() 
    {
        super();
        // TODO Auto-generated constructor stub
    }

    @Override
	public void init() throws ServletException
	{
		System.out.println("Checkout-init");
		super.init();
	}
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		HttpSession sn = request.getSession();
		HttpAuth auth = new HttpAuth();
		if(sn.getAttribute("identity") == null)
		{
			this.getServletContext().getNamedDispatcher("Login").forward(request, response);
		}
		else
		{
			LinkedList<CartItem> cart;
			if(sn.getAttribute("cart") == null)
			{
				cart = new LinkedList<CartItem>();
				sn.setAttribute("cart", cart);
			}
			else
			{
				@SuppressWarnings("unchecked")
				LinkedList<CartItem> attribute = (LinkedList<CartItem>) sn.getAttribute("cart");
				cart = attribute;
			}
			request.setAttribute("cart", cart);
			
			String target = "/Front.jspx";
			request.setAttribute("target", "Checkout.jspx");
			RequestDispatcher rd = request.getRequestDispatcher(target);
			rd.forward(request, response);
		}
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		this.doGet(request, response);
	}

}
