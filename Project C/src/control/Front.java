package control;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.swing.JOptionPane;

import model.CartItem;
import model.CategoryBean;
import model.CategoryDAO;
import model.Customer;
import model.ItemBean;
import model.ItemDAO;
import model.OrderProducer;

/**
 * Servlet implementation class Front
 */
@WebServlet(urlPatterns = {"/Front/", "/Front/*"})
public class Front extends HttpServlet
{
	private static final long serialVersionUID = 1L;

	@Override
	public void init() throws ServletException
	{
		super.init();
		System.out.println("Front-init");
		this.getServletContext().setAttribute("DAO", new ItemDAO());
		this.getServletContext().setAttribute("CatDAO", new CategoryDAO());
	}

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Front()
	{
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@SuppressWarnings("unused")
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		HttpSession sn = request.getSession();
		Customer customer = (Customer) sn.getAttribute("identity");
		int counter = 0;
		
		if(request.getParameter("logout") != null)
		{
			sn.setAttribute("identity", null);
			request.setAttribute("loginDef", null);
			counter = 0;
		}
		else
		{
			//I'll comment this out when we get authentication working
			/*if(customer == null)
			{
				customer = new Customer("cse13148", "Dallis King");
				sn.setAttribute("identity", customer);
			}*/
			
			if(customer != null)
			{
				request.setAttribute("loginDef", customer);
				counter++;
			}
		}
		
		System.out.println("In Front--begin: *****************");
		System.out.println("url = " + request.getRequestURL());
		System.out.println("uri = " + request.getRequestURI());
		System.out.println("pth = " + request.getPathInfo());
		System.out.println("In Front--finish *****************\n");
		

		LinkedList<CartItem> procurment = null;
		LinkedList<CartItem> cart = null;
		List<ItemBean> list = null;
		List<CategoryBean> catList = null;

		@SuppressWarnings("unchecked")
		LinkedList<CartItem> att = (LinkedList<CartItem>) sn.getAttribute("cart");
		cart = att;
		request.setAttribute("allItems", cart);
		
		CategoryDAO catTest = (CategoryDAO) this.getServletContext().getAttribute("CatDAO");
		try
		{
			catList = catTest.retreieve();
			
			/*for(CategoryBean c: catList)
			{
				System.out.println(c.getID() + " " + c.getName() + " " + c.getDescription() + " " + c.getURL());
			}
			*/
		}
		catch (Exception e)
		{
			System.out.println("error in cat: " + e.getMessage());
		}
		
		ItemDAO test = (ItemDAO) this.getServletContext().getAttribute("DAO");
		try
		{
			list = test.retreieve();
			/*List<ItemBean> testThing = test.retreieve("beef"); 
			
			for(ItemBean c: testThing)
			{
				System.out.println(c.getUnit() + " " + c.getCostprice() + " " + c.getSupid() + " " +
						c.getCatid() + " " + c.getReorder() + " " + c.getOnorder() + " " + c.getQty() +
						" " + c.getPrice() + " " + c.getName() + " " + c.getNumber());
			}*/
		}
		catch (Exception e)
		{
			System.out.println("error in item: " + e.getMessage());
		}
		
		if (request.getPathInfo() != null && request.getPathInfo().equals("/Meat"))
		{
			System.out.println("In Meat");
			request.setAttribute("ticket", "F-to-Meat");
			request.setAttribute("list", list);
			this.getServletContext().getNamedDispatcher("Meat").forward(request, response);
		} 
		else if (request.getPathInfo() != null && request.getPathInfo().equals("/Cheese"))
		{
			System.out.println("In Cheese");
			request.setAttribute("ticket", "F-to-Cheese");
			request.setAttribute("list", list);
			this.getServletContext().getNamedDispatcher("Cheese").forward(request, response);
		} 
		else if (request.getPathInfo() != null && request.getPathInfo().equals("/IceCream"))
		{
			System.out.println("In IceCream");
			request.setAttribute("ticket", "F-to-IceCream");
			request.setAttribute("list", list);
			this.getServletContext().getNamedDispatcher("IceCream").forward(request, response);
		} 
		else if (request.getPathInfo() != null && request.getPathInfo().equals("/Cereal"))
		{
			System.out.println("In Cereal");
			request.setAttribute("ticket", "F-to-Cereal");
			request.setAttribute("list", list);
			this.getServletContext().getNamedDispatcher("Cereal").forward(request, response);
		}
		else if (request.getPathInfo() != null && request.getPathInfo().equals("/OrderDone"))
		{
			//Customer custom = (Customer) sn.getAttribute("identity");
			OrderProducer prod = new OrderProducer();
			if(request.getParameter("continue") != null)
			{
				System.out.println("We're in continue");
				request.setAttribute("ticket", "Front");
				request.setAttribute("list", catList);
				request.getRequestDispatcher("/Front.jspx").forward(request, response);
			}
			else
			{
				String filename = this.getServletContext().getRealPath("/export") + "/po" +customer.getAccountNo() + "_" + counter + ".xml";
				File file = new File(filename);
				while(file.exists())
				{
					counter++;
					filename = this.getServletContext().getRealPath("/export") + "/po" +customer.getAccountNo() + "_" + counter + ".xml";
					file = new File(filename);
				}
				try
				{
					prod.generateOrder(cart, customer, file);
					request.setAttribute("linkToXML", "/po" +customer.getAccountNo() + "_" + counter + ".xml");
					if(procurment == null){
						System.out.println("procurment is null");
						procurment = cart;
					}
					else
					{
						System.out.println("procurment is not null");
						for(CartItem e: cart){
							System.out.print("adding to procurment:");
							procurment.add(e);
							System.out.println();
						}
					}
					for(CartItem e: procurment)
					{
						System.out.println(e.getName() + " " + e.getNumber() + " " + e.getPricePerItem() + " " + e.getQty() + " " + e.getTotalPrice());
					}
					sn.setAttribute("procurment", procurment);
					sn.setAttribute("cart", new LinkedList<CartItem>());
					System.out.println("OrderDone");
					request.setAttribute("ticket", "OrderDone");
					request.setAttribute("list", list);
					counter++;
			
					this.getServletContext().getNamedDispatcher("OrderDone").forward(request, response);
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
			}
		}
		else if (request.getPathInfo() != null && request.getPathInfo().equals("/Cart"))
		{
			System.out.println("In Cart");
			if(request.getParameter("continue") != null)
			{
				System.out.println("We're in continue");
				request.setAttribute("ticket", "Front");
				request.setAttribute("list", catList);
				request.getRequestDispatcher("/Front.jspx").forward(request, response);
			}
			/*else if(request.getParameter("checkout") != null)
			{
				System.out.println("We're in checkout");
				request.setAttribute("ticket", "Login");
				request.setAttribute("list", catList);
				request.getRequestDispatcher("/Login.jspx").forward(request, response);
				this.getServletContext().getNamedDispatcher("Cart").forward(request, response);
			}*/
			else if(request.getParameter("update") != null)
			{
				System.out.println("We're in update");
				@SuppressWarnings("unchecked")
				LinkedList<CartItem> attribute = (LinkedList<CartItem>) sn.getAttribute("cart");
				cart = attribute;
				
				System.out.println("Before for loop");
				for(CartItem x : cart){
					System.out.println("In For Loop");
					String qty = request.getParameter(x.getNumber());	
					System.out.println("quantity = "+ qty);
					if(!qty.matches("-?\\d+(.\\d+)?")){
						JOptionPane.showMessageDialog(null, "please enter numbers only", "Error",
                                JOptionPane.ERROR_MESSAGE);
					}
					else{
						int quantity = Integer.parseInt(qty);
						if(quantity != 0){
							try {
								x.setQty(qty);
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
						else{
							cart.remove(x);
						}
					}
				}		
				System.out.println("After for loop");
				this.getServletContext().getNamedDispatcher("Cart").forward(request, response);
			}
			else{
				request.setAttribute("ticket", "F-to-Cart");
				request.setAttribute("list", list);
				this.getServletContext().getNamedDispatcher("Cart").forward(request, response);
			}
		}
		else if (request.getPathInfo() != null && request.getPathInfo().equals("/Login"))
		{
			System.out.println("In Login");
			request.setAttribute("ticket", "Login");
			request.setAttribute("list", list);		
			if(request.getParameter("continue") != null)
			{
				System.out.println("We're in update");
				request.setAttribute("ticket", "Front");
				request.setAttribute("list", catList);
				request.getRequestDispatcher("/Front.jspx").forward(request, response);
			}
			else{
				this.getServletContext().getNamedDispatcher("Login").forward(request, response);
			}
		} 
		else if (request.getPathInfo() != null && request.getPathInfo().equals("/Checkout"))
		{
			System.out.println("In Checkout");
			request.setAttribute("ticket", "Checkout");
			request.setAttribute("list", list);
			this.getServletContext().getNamedDispatcher("Checkout").forward(request, response);
		}
		else if (request.getPathInfo() != null && request.getPathInfo().equals("/Search"))
		{
			System.out.println("Feel the: " + request.getParameter("searchable"));
			try
			{
				List<ItemBean> searchList = test.retreieve(request.getParameter("searchable"));	
				request.setAttribute("searchList", searchList);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			System.out.println("In Search");
			request.setAttribute("ticket", "Search");
			request.setAttribute("list", list);
			this.getServletContext().getNamedDispatcher("Search").forward(request, response);
		} 
		else
		{
			System.out.println("In Front");
			request.setAttribute("ticket", "Front");
			request.setAttribute("list", catList);
			request.getRequestDispatcher("/Front.jspx").forward(request, response);
		}
		
		if(sn.getAttribute("cart") == null)
		{
			cart = new LinkedList<CartItem>();
			sn.setAttribute("cart", cart);
		}
		else
		{
			@SuppressWarnings("unchecked")
			LinkedList<CartItem> attribute = (LinkedList<CartItem>) sn.getAttribute("cart");
			cart = attribute;
			request.setAttribute("allItems", cart);
		}
		
		for(CartItem e: cart)
		{
			System.out.println(e.getName() + " " + e.getNumber() + " " + e.getPricePerItem() + " " + e.getQty() + " " + e.getTotalPrice());
		}
		
		if(request.getParameter("action") != null)
		{
			System.out.println("adding to cart...");
			try
			{
				String num = request.getParameter("item");
				String qty = request.getParameter("qty");
				boolean con = false;
				boolean pcon = false;
				
				System.out.println("CART: "+ cart);
				for(CartItem x : cart){
					System.out.println("x num = " + x.getNumber());
					System.out.println("adding num = " + num);
					if (num.equals(x.getNumber())){
						System.out.println("they match!");
						System.out.println("x qty = " + x.getQty());
						System.out.println("adding qty = " + qty);
						int qnty = Integer.parseInt(qty);
						int together = x.getQty() + qnty;
						System.out.println("together = "+together);
						qty = ""+together;
						x.setQty(qty);
						System.out.println("x's new quantity = " + x.getQty());
						con = true;
						sn.setAttribute("number", num);
						sn.setAttribute("allItems", cart);
					}
				}
				
				if(con == false){
					System.out.println(num+ " " + qty);
					CartItem e = new CartItem(num, qty);
					System.out.println(e.getName() + " " + e.getNumber() + " " + e.getPricePerItem() + " " + e.getQty() + " " + e.getTotalPrice());
					cart.add(e);
					sn.setAttribute("cart", cart);
					sn.setAttribute("allItems", cart);
					sn.setAttribute("number", num);
				}
			}
			catch (Exception e)
			{
				System.out.println(e.toString());
				request.setAttribute("error", e);
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException
	{
		this.doGet(request, response);
	}

}

