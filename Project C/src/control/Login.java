package control;

import java.io.IOException;
import java.util.LinkedList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.Customer;
import auth.HttpAuth;

/**
 * Servlet implementation class Cart
 */
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Login()
    {
        super();
        // TODO Auto-generated constructor stub
    }

    @Override
	public void init() throws ServletException
	{
		System.out.println("Login-init");
		super.init();
	}
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		HttpSession sn = request.getSession();
		HttpAuth auth = new HttpAuth();
		
		request.setAttribute("loginDef", sn.getAttribute("identity"));
		
		String target = "/Front.jspx";
		request.setAttribute("target", "Login.jspx");
		RequestDispatcher rd = request.getRequestDispatcher(target);
		rd.forward(request, response);
		
		if(request.getParameter("continue") != null)
		{
			System.out.println("We're in update");
			request.setAttribute("ticket", "Front");
			//request.setAttribute("list", catList);
			request.getRequestDispatcher("/Front.jspx").forward(request, response);
		}
		
		if(request.getParameter("login") != null)
		{
			try
			{
				String acct = request.getParameter("user");
				String name = auth.login(acct, request.getParameter("pass"));
				System.out.println(name);
				Customer cust = new Customer(acct, name);
				sn.setAttribute("identity", cust);
				request.setAttribute("loginDef", cust);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		this.doGet(request, response);
	}
}
