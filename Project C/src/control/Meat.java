package control;
import java.io.IOException;
import java.util.LinkedList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.CartItem;

public class Meat extends HttpServlet
{
	@Override
	public void init() throws ServletException
	{
		System.out.println("Meat-init");
		super.init();
	}

	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		String target = "/Front.jspx";
		request.setAttribute("target", "Meat.jspx");
		RequestDispatcher rd = request.getRequestDispatcher(target);
		rd.forward(request, response);
		
//		HttpSession sn = request.getSession();
//		LinkedList<CartItem> cart;
//		if(sn.getAttribute("cart") == null)
//		{
//			System.out.println("We are here, dude");
//			cart = new LinkedList<CartItem>();
//			sn.setAttribute("cart", cart);
//		}
//		else
//		{
//			@SuppressWarnings("unchecked")
//			LinkedList<CartItem> attribute = (LinkedList<CartItem>) sn.getAttribute("cart");
//			cart = attribute;
//		}
//		for(CartItem e: cart)
//		{
//			System.out.println(e.getName() + " " + e.getNumber() + " " + e.getPricePerItem() + " " + e.getQty() + " " + e.getTotalPrice());
//		}
//		
//		if(request.getParameter("action") != null)
//		{
//			try
//			{
//				String num = (String) request.getParameter("item");
//				String qty = request.getParameter("qty");
//				cart.add(new CartItem(num, qty));
//				sn.setAttribute("cart", cart);
//			}
//			catch (Exception e)
//			{
//				request.setAttribute("error", e);
//			}
//		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException
	{
		this.doGet(request, response);
	}
}
