package filter;

import java.io.IOException;
import java.util.LinkedList;

import javax.servlet.DispatcherType;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;

import model.CartItem;

/**
 * Servlet Filter implementation class adhoc
 * Unfinished. No time.
 */
@WebFilter(dispatcherTypes = {DispatcherType.FORWARD}, servletNames = { "Cheese","Meat","Cereal","IceCream" })
public class adhoc implements Filter {
	public adhoc() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		
		// do I want to interfere?
		// if so then
		//     interfere and forward jsp
		// else
		//     chain.doFilter(request, response);
		// here you have a second chance to interfere (on the way back)
		
		System.out.println("In Filter");
		@SuppressWarnings("unchecked")
		LinkedList<CartItem> cart = (LinkedList<CartItem>) request.getAttribute("cart");
		String number = request.getParameter("number");
		System.out.println(number);
		if(number != null){
			System.out.println(number);
			if(number.equals("1409S413")){
				CartItem e = null;
				try {
					e = new CartItem(number,"1");
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				cart.add(e);
			}
		}
		System.out.println("out of filter");

		chain.doFilter(request, response);
	
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}
}
