package model;


import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder={"name", "price", "quantity", "extended"})
public class CartItem
{
	@XmlAttribute
	private String number;
	@XmlElement
	private String name;
	@XmlElement
	private int quantity;
	@XmlElement
	private double price;
	@XmlElement
	private double extended;
	
	
	public CartItem(String num, String qty) throws Exception
	{
		//System.out.println("Houston");
		this.number = num;
		ItemDAO dao = new ItemDAO();
		
		this.quantity = safeConvert(qty);
		
		System.out.println(this.number);
		System.out.println(this.quantity);
			
		ItemBean bean = dao.cartItemByNumber(num);
		System.out.println(bean != null);
			
		this.name = bean.getName();
		this.price = bean.getPrice();
		this.extended = this.price * this.quantity;
		
		System.out.println(this.name);
		
		System.out.println(this.price);
		System.out.println(this.extended);
	}


	public int getQty()
	{
		return quantity;
	}

	public void setQty(String qty) throws Exception
	{
		this.quantity = safeConvert(qty);
		this.extended = this.price * this.quantity;
	}

	public String getNumber()
	{
		return number;
	}


	public String getName()
	{
		return name;
	}


	public double getPricePerItem()
	{
		return price;
	}


	public double getTotalPrice()
	{
		return extended;
	}
	
	public int safeConvert(String a) throws Exception
	{
		return Integer.parseInt(a);
	}
	
}
