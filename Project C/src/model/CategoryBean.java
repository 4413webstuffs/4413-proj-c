package model;

import java.io.OutputStream;

public class CategoryBean
{
	int id = 0;
	String name = null;
	String description = null;
	String url = null;
	OutputStream file = null;
	
	public CategoryBean()
	{
		//NOTHING!!
	}
	
	public CategoryBean(int id, String name, String description, String url, OutputStream file)
	{
		this.id = id;
		this.name = name;
		this.description = description;
		this.url = url;
		this.file = file;
	}

	public int getID()
	{
		return id;
	}

	public void setID(int id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}
	public String getURL()
	{
		return url;
	}

	public void setURL(String url)
	{
		this.url = url;
	}
	public OutputStream getFile()
	{
		return file;
	}

	public void setFile(OutputStream file)
	{
		this.file = file;
	}
}

