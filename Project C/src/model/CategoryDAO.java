package model;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

public class CategoryDAO
{
	private final String dbURL = "jdbc:derby://roumani.eecs.yorku.ca:64413/CSE;user=student;password=secret;";
	
	public CategoryDAO()
	{
		try
		{
			Class.forName("org.apache.derby.jdbc.ClientDriver").newInstance();
		}
		catch (Exception e)
		{
			System.out.println("Something awful happened");
		}
	}
	
	
	public List<CategoryBean> retreieve() throws SQLException, IOException
	{
		Connection conn = DriverManager.getConnection(dbURL);
		Statement stmt = conn.createStatement();
		stmt.executeUpdate("set schema roumani");
		ResultSet results = stmt.executeQuery("select * from category");
		
		
		List<CategoryBean> list = new LinkedList<CategoryBean>();
		
		ResultSetMetaData rsmd = results.getMetaData();
		int columnsNumber = rsmd.getColumnCount();
		
		while(results.next())
		{
			CategoryBean bn = new CategoryBean();
			
			int id = 0;
			String name = null;
			String description = null;
			String url = null;
			OutputStream file = null;
			
			for(int i = 1; i <= columnsNumber; i++)
			{
				String cat = rsmd.getColumnName(i);
				if(cat.equals("ID"))
				{
					id = results.getInt(i);
				}
				if(cat.equals("NAME"))
				{
					name = results.getString(i);
					url = results.getString(i).replaceAll("\\s", "");
				}
				if(cat.equals("DESCRIPTION"))
				{
					description = results.getString(i);
				}
				if(cat.equals("PICTURE"))
				{
					byte[] fileBytes;
	                fileBytes = results.getBytes(i);
	                file =  new FileOutputStream(url+".png");
	                file.write(fileBytes);
	                file.close();
	             }        
			}
			bn.setID(id);
			bn.setName(name);
			bn.setDescription(description);
			bn.setURL(url);
			bn.setFile(file);
			
			list.add(bn);
		}
		return list;
	}
}

