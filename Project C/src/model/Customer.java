package model;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

public class Customer
{
	@XmlAttribute
	private String account;
	@XmlElement
	private String name;
	
	public Customer(String acc, String name)
	{
		this.account = acc;
		this.name = name;
	}

	public String getAccountNo()
	{
		return account;
	}
	
	public void setAccount(String account)
	{
		this.account = account;
	}
	
	public String getFullName()
	{
		return name;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
}
