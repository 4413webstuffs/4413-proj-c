package model;

public class ItemBean
{
	private String unit;
	private double costprice;
	private int supid;
	private int catid;
	private int reorder;
	private int onorder;
	private int qty;
	private double price;
	private String name;
	private String number;
	
	
	public ItemBean()
	{
		//NOTHING!!
	}
	
	public ItemBean(String u, double cp, int sup, int cat, int reo, int ono, int qty, double price, String name, String number)
	{
		this.unit = u;
		this.costprice = cp;
		this.supid = sup;
		this.catid = cat;
		this.reorder = reo;
		this.onorder = ono;
		this.qty = qty;
		this.price = price;
		this.name = name;
		this.number = number;
	}

	public String getUnit()
	{
		return unit;
	}

	public void setUnit(String unit)
	{
		this.unit = unit;
	}

	public double getCostprice()
	{
		return costprice;
	}

	public void setCostprice(double costprice)
	{
		this.costprice = costprice;
	}

	public int getSupid()
	{
		return supid;
	}

	public void setSupid(int supid)
	{
		this.supid = supid;
	}

	public int getCatid()
	{
		return catid;
	}

	public void setCatid(int catid)
	{
		this.catid = catid;
	}

	public int getReorder()
	{
		return reorder;
	}

	public void setReorder(int reorder)
	{
		this.reorder = reorder;
	}

	public int getOnorder()
	{
		return onorder;
	}

	public void setOnorder(int onorder)
	{
		this.onorder = onorder;
	}

	public int getQty()
	{
		return qty;
	}

	public void setQty(int qty)
	{
		this.qty = qty;
	}

	public double getPrice()
	{
		return price;
	}

	public void setPrice(double price)
	{
		this.price = price;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getNumber()
	{
		return number;
	}

	public void setNumber(String number)
	{
		this.number = number;
	}
}
