package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

public class ItemDAO
{
	private final String dbURL = "jdbc:derby://roumani.eecs.yorku.ca:64413/CSE;user=student;password=secret;";
	
	public ItemDAO()
	{
		try
		{
			Class.forName("org.apache.derby.jdbc.ClientDriver").newInstance();
		}
		catch (Exception e)
		{
			System.out.println("Something awful happened");
		}
	}
	
	
	public List<ItemBean> retreieve() throws SQLException
	{
		Connection conn = DriverManager.getConnection(dbURL);
		Statement stmt = conn.createStatement();
		stmt.executeUpdate("set schema roumani");
		ResultSet results = stmt.executeQuery("select * from item");
		
		
		List<ItemBean> list = new LinkedList<ItemBean>();
		
		ResultSetMetaData rsmd = results.getMetaData();
		int columnsNumber = rsmd.getColumnCount();
		
		while(results.next())
		{
			ItemBean bn = new ItemBean();
			
			String unit = null;
			double costprice = 0;
			int supid = 0;
			int catid = 0;
			int reorder = 0;
			int onorder = 0;
			int qty = 0;
			double price = 0;
			String name = null;
			String number = null;
			
			for(int i = 1; i <= columnsNumber; i++)
			{
				String cat = rsmd.getColumnName(i);
				if(cat.equals("UNIT"))
				{
					unit = results.getString(i);
				}
				if(cat.equals("COSTPRICE"))
				{
					costprice = results.getDouble(i);
				}
				if(cat.equals("SUPID"))
				{
					supid = results.getInt(i);
				}
				if(cat.equals("CATID"))
				{
					catid = results.getInt(i);
				}
				if(cat.equals("REORDER"))
				{
					reorder = results.getInt(i);
				}
				if(cat.equals("ONORDER"))
				{
					onorder = results.getInt(i);
				}
				if(cat.equals("QTY"))
				{
					qty = results.getInt(i);
				}
				if(cat.equals("PRICE"))
				{
					price = results.getDouble(i);
				}
				if(cat.equals("NAME"))
				{
					name = results.getString(i);
				}
				if(cat.equals("NUMBER"))
				{
					number = results.getString(i);
				}
			}
			bn.setCatid(catid);
			bn.setName(name);
			bn.setCostprice(costprice);
			bn.setOnorder(onorder);
			bn.setPrice(price);
			bn.setQty(qty);
			bn.setReorder(reorder);
			bn.setSupid(supid);
			bn.setUnit(unit);
			bn.setNumber(number);
			
			list.add(bn);
		}
		return list;
	}
	
	public ItemBean cartItemByNumber(String number) throws SQLException
	{
		Connection conn = DriverManager.getConnection(dbURL);
		Statement stmt = conn.createStatement();
		stmt.executeUpdate("set schema roumani");
		ResultSet results = stmt.executeQuery("select * from item where number LIKE '"+ number + "'");
		
		ResultSetMetaData rsmd = results.getMetaData();
		int columnsNumber = rsmd.getColumnCount();
		
		ItemBean bn = new ItemBean();
		bn.setNumber(number);
		
		results.next();
		
		for(int i = 1; i <= columnsNumber; i++)
		{
			String cat = rsmd.getColumnName(i);
			if(cat.equals("UNIT"))
			{
				bn.setUnit(results.getString(i));
				//System.out.println("results =" + results.getString(i));
			}
			if(cat.equals("COSTPRICE"))
			{
				bn.setCostprice(results.getDouble(i));
				//System.out.println(results.getString(i));
			}
			if(cat.equals("SUPID"))
			{
				bn.setSupid(results.getInt(i));
				//System.out.println(results.getString(i));
			}
			if(cat.equals("CATID"))
			{
				bn.setCatid(results.getInt(i));
				//System.out.println(results.getString(i));
			}
			if(cat.equals("REORDER"))
			{
				bn.setReorder(results.getInt(i));
				//System.out.println(results.getString(i));
			}
			if(cat.equals("ONORDER"))
			{
				bn.setOnorder(results.getInt(i));
				//System.out.println(results.getString(i));
			}
			if(cat.equals("QTY"))
			{
				bn.setQty(results.getInt(i));
				//System.out.println(results.getString(i));
			}
			if(cat.equals("PRICE"))
			{
				bn.setPrice(results.getDouble(i));
				//System.out.println(results.getString(i));
			}
			if(cat.equals("NAME"))
			{
				bn.setName(results.getString(i));
			}
		}
		return bn;
	}
	
	public List<ItemBean> retreieve(String san) throws SQLException
	{
		Connection conn = DriverManager.getConnection(dbURL);
		Statement stmt = conn.createStatement();
		stmt.executeUpdate("set schema roumani");
		ResultSet results = stmt.executeQuery("select * from item where UPPER(NAME) like UPPER('%" + san + "%') or UPPER(NUMBER) like UPPER('%" + san + "%')");
		
		
		List<ItemBean> list = new LinkedList<ItemBean>();
		
		ResultSetMetaData rsmd = results.getMetaData();
		int columnsNumber = rsmd.getColumnCount();
		
		while(results.next())
		{
			ItemBean bn = new ItemBean();
			
			String unit = null;
			double costprice = 0;
			int supid = 0;
			int catid = 0;
			int reorder = 0;
			int onorder = 0;
			int qty = 0;
			double price = 0;
			String name = null;
			String number = null;
			
			for(int i = 1; i <= columnsNumber; i++)
			{
				String cat = rsmd.getColumnName(i);
				if(cat.equals("UNIT"))
				{
					unit = results.getString(i);
				}
				if(cat.equals("COSTPRICE"))
				{
					costprice = results.getDouble(i);
				}
				if(cat.equals("SUPID"))
				{
					supid = results.getInt(i);
				}
				if(cat.equals("CATID"))
				{
					catid = results.getInt(i);
				}
				if(cat.equals("REORDER"))
				{
					reorder = results.getInt(i);
				}
				if(cat.equals("ONORDER"))
				{
					onorder = results.getInt(i);
				}
				if(cat.equals("QTY"))
				{
					qty = results.getInt(i);
				}
				if(cat.equals("PRICE"))
				{
					price = results.getDouble(i);
				}
				if(cat.equals("NAME"))
				{
					name = results.getString(i);
				}
				if(cat.equals("NUMBER"))
				{
					number = results.getString(i);
				}
			}
			bn.setCatid(catid);
			bn.setName(name);
			bn.setCostprice(costprice);
			bn.setOnorder(onorder);
			bn.setPrice(price);
			bn.setQty(qty);
			bn.setReorder(reorder);
			bn.setSupid(supid);
			bn.setUnit(unit);
			bn.setNumber(number);
			
			list.add(bn);
		}
		return list;
	}
	
}
