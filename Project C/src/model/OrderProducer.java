package model;

import java.io.File;
import java.io.FileWriter;
import java.io.StringWriter;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.transform.stream.StreamResult;

public class OrderProducer
{
	@XmlRootElement(name = "order")
	public static class Order
	{
		@XmlAttribute
		private int id;
		@XmlAttribute
		private Date submitted;
		@XmlElement
		private Customer customer;
		@XmlElement(name="items")
		private List<CartItem> list;
		@XmlElement
		private double total;
		@XmlElement
		private double shipping;
		@XmlElement
		private double HST;
		@XmlElement
		private double grandTotal;
		
		public Order()
		{
			//Do not use.
		}
		
		public Order(List<CartItem> list, Customer customer)
		{
			Random rn = new Random();
			this.id = rn.nextInt();
			this.submitted = Calendar.getInstance().getTime();
			this.customer = null;
			this.customer = customer;
			this.list = list;
			double temp = 0;
			for(CartItem cItem: list)
			{
				temp += cItem.getTotalPrice();
			}
			this.total = temp;
			this.shipping = 5.0;
			
			if(temp >= 100)
			{
				this.shipping = 0.0;
			}
			
			this.HST = (temp + this.shipping) * 0.13;
			
			this.grandTotal = this.HST + temp + this.shipping;
		}
		
		public List<CartItem> getTheItems()
		{
			return this.list;
		}
		
	}
	
	public void generateOrder(List<CartItem> order, Customer customer, File filename) throws Exception
	{
		Order lw = new Order(order, customer);
		JAXBContext jc = JAXBContext.newInstance(lw.getClass());
		Marshaller marshaller = jc.createMarshaller();
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		
		marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
		
		StringWriter sw = new StringWriter();
		sw.write("<?xml version=\"1.0\" ?>\n"); 
		sw.write("<?xml-stylesheet type=\"text/xsl\" href=\"/Project_C/res/OneOrder.xsl\"?>\n");
		marshaller.marshal(lw, new StreamResult(sw));
		  
		System.out.println(sw.toString()); // for debugging
		
		
		FileWriter fw = new FileWriter(filename);
		fw.write(sw.toString());
		fw.close();
	}
}
